![screenshot](screenshot/dwm_1.png)
![screenshot](screenshot/dwm_2.png)

**Patches applied:**
- [dwm-systray-6.2.diff](https://dwm.suckless.org/patches/systray/dwm-systray-6.2.diff)
- [dwm-pertag-6.2.diff](https://dwm.suckless.org/patches/pertag/dwm-pertag-6.2.diff)
- [dwm-autostart-20200610-cb3f58a.diff](https://dwm.suckless.org/patches/autostart/dwm-autostart-20161205-bb3bd6f.diff) ( autostart.sh ~/.local/share/dwm ~/.config/dwm )
- [replace-dmenu-rofi](https://slackbuilds.org/slackbuilds/14.2/desktop/dwm/sbo-patches/replace-dmenu-rofi.patch)
- [dwm-status2d-6.2.diff](https://dwm.suckless.org/patches/status2d/dwm-status2d-6.2.diff)

**status bar:** [slstatus](https://git.suckless.org/slstatus)
- config.h -> slstatus.config.h

**Font:**   Iosevka

**Hot key:**
- PrtSc             		screenshots(maim) Full Screen   ---> /tmp/Fullscreen_*.png
- Shift + PrtSc     		screenshots (maim)              ---> /tmp/Shot_*.png
- Alt + Shift + PrtSc       Screenshot(maim) Active window  ---> /tmp/Wshot_*.png

